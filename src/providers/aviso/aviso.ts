import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';

/*
  Generated class for the AvisoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AvisoProvider {
  private token: { 'Authorization': string; };

  constructor(private api: ApiProvider) {
    this.token = { 'Authorization': window.sessionStorage.getItem('token') };
  }

  index(): Promise<any> {
    return this.api.get(`avisos`, null, this.token);
  }

  show(id: number): Promise<any> {
    return this.api.get(`avisos/${id}`, null, this.token);
  }

  read(id: number): Promise<any> {
    return this.api.post(`avisos/user/adicionar`, { aviso_id: id }, this.token);
  }

  lidos() {
    return this.api.get(`avisos/user/lidos`, null, this.token);
  }
}
