import { Profile } from './../../models/user.model';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  _user: Profile;
  constructor(private http: HttpClient, private storage: Storage, private events: Events) {
    events.subscribe('auth:login', (data: { token: string; }) => {
      window.sessionStorage.setItem('token', `Bearer ${data.token}`);
      this.setToken(data.token)
        .then(() => {
          this._getProfile();
        });
    });

    events.subscribe('user:logout', () => {
      this.logout();
    });

    this.storage.get('_user')
      .then((profile) => {
        if (profile) {
          this._user = profile;
          this.events.publish('auth:me', this._user);
        }
      });

  }

  public user(): Profile {
    return this._user;
  }

  login(params: { username: string, password: string }) {
    return this.http.post(`${environment.apiUrl}login`, params).toPromise();
  }

  setToken(token: string): Promise<void> {
    window.sessionStorage.setItem('token', `Bearer ${token}`);
    return this.storage.set('token', token)
  }

  isAuth(): Promise<boolean> {
    return this.storage.get('token')
      .then((token) => {
        if (token) {
          window.sessionStorage.setItem('token', `Bearer ${token}`);
          this._getProfile();
          return true;
        } else {
          return false;
        }
      });
  }


  private _saveUser() {
    this.events.publish('auth:me', this._user);
    this.storage.set('_user', this._user);
  }

  private _getProfile(): Promise<any | HttpErrorResponse> {
    return this.http.get(`${environment.apiUrl}user/profile`, {
      headers: new HttpHeaders({ 'Authorization': window.sessionStorage.getItem('token') })
    }).toPromise()
      .then((data: Profile) => {
        this._user = data;
        this._saveUser();
        return data;
      }).catch((err: HttpErrorResponse) => {
        console.log(err);

        this.storage.get('_user')
          .then((_user: Profile) => {
            if (_user) {
              this._user = _user;
              this._saveUser();
            }
          })

        if (err.status == 401) {
          this.events.publish('alert:create', {
            title: 'Ops!',
            subTitle: 'É necessário fazer logout!',
            buttons: [{
              text: 'OK',
              handler: () => {
                this.events.publish('auth:logout');
              }
            }]
          });
        }
        return err;
      });
  }

  logout(): Promise<void> {
    this.events.publish('loading:create', {
      content: 'Saindo...'
    });
    this._getProfile().then(() => {
      this.events.publish('loading:dismiss');
      this.events.publish('auth:logout');
    });
    return this.storage.clear();

  }
}
