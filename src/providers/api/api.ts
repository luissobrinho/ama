import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class ApiProvider {
  url: string = environment.apiUrl;

  constructor(public http: HttpClient) {

  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return this.http.get(`${this.url}${endpoint}`, {
      headers: new HttpHeaders(reqOpts)
    }).toPromise();
  }

  post(endpoint: string, body: any, reqOpts?: {}) {
    return this.http.post(`${this.url}${endpoint}`, body, {
      headers: new HttpHeaders(reqOpts)
    }).toPromise();
  }

  put(endpoint: string, body: any, reqOpts?: {}) {
    return this.http.put(`${this.url}${endpoint}`, body, {
      headers: new HttpHeaders(reqOpts)
    }).toPromise();
  }

  delete(endpoint: string, reqOpts?: {}) {
    return this.http.delete(`${this.url}${endpoint}`, {
      headers: new HttpHeaders(reqOpts)
    }).toPromise();
  }

  patch(endpoint: string, body: any, reqOpts?: {}) {
    return this.http.patch(`${this.url}${endpoint}`, body, {
      headers: new HttpHeaders(reqOpts)
    }).toPromise();
  }
}
