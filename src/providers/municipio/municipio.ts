import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';

/*
  Generated class for the MunicipioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MunicipioProvider {
  private token: { 'Authorization': string; };

  constructor(private api: ApiProvider) {
    this.token = { 'Authorization': window.sessionStorage.getItem('token') };
  }

  index(): Promise<any> {
    return this.api.get(`municipios`, null, this.token);
  }

  show(id: number): Promise<any> {
    return this.api.get(`municipios/${id}`, null, this.token);
  }

}
