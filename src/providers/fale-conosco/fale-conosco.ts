import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';

/*
  Generated class for the FaleConoscoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FaleConoscoProvider {

  private token: { 'Authorization': string; };

  constructor(private api: ApiProvider) {
    this.token = { 'Authorization': window.sessionStorage.getItem('token') };
  }

  index(): Promise<any> {
    return this.api.get(`mensagens`, null, this.token);
  }

  show(id: number): Promise<any> {
    return this.api.get(`mensagens/${id}`, null, this.token);
  }

  store(parans: any): Promise<any> {
    return this.api.post(`mensagens`, parans, this.token);
  }

  reply(parans: any): Promise<any> {
    return this.api.post(`mensagens/reply`, parans, this.token);
  }

  assunto(): Promise<any> {
    return this.api.get(`mensagens/assuntos`, null, this.token);
  }
}
