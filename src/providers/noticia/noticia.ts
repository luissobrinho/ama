import { ApiProvider } from './../api/api';
import { Injectable } from '@angular/core';

/*
  Generated class for the NoticiaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NoticiaProvider {
  private token: { 'Authorization': string; };

  constructor(private api: ApiProvider) {
    this.token = { 'Authorization': window.sessionStorage.getItem('token') };
  }

  index(): Promise<any> {
    return this.api.get(`noticias`, null, this.token);
  }

  show(id: number): Promise<any> {
    return this.api.get(`noticias/${id}`, null, this.token);
  }

  byCategoria(categoria_id: number) {
    return this.api.get(`noticias/categorias/${categoria_id}`, null, this.token);
  }

  indexCategoria(): Promise<any> {
    return this.api.get(`noticias/categorias`, null, this.token);
  }
}
