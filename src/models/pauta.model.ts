export interface Pautas {
  current_page: number;
  data: Pauta[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url?: any;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface Pauta {
  id: number;
  titulo: string;
  data: string;
  publicar: string;
  created_at: string;
  updated_at: string;
  deleted_at?: any;
  assuntos: Assunto[];
}

interface Assunto {
  id: number;
  pauta_id: string;
  descricao: string;
  created_at: string;
  updated_at: string;
}