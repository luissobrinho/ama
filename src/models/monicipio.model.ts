export interface Municipios {
  current_page: number;
  data: Municipio[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface Municipio {
  id: number;
  nome: string;
  cnpj?: any;
  data_emancipacao?: any;
  site?: any;
  email?: any;
  telefone?: any;
  fpm?: any;
  ibge: string;
  created_at?: any;
  updated_at?: any;
  deleted_at?: any;
  usuarios: any[];
  populacoes: any[];
}