interface Mensagens {
    current_page: number;
    data: Mensagem[];
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: string;
    next_page_url?: any;
    path: string;
    per_page: number;
    prev_page_url?: any;
    to: number;
    total: number;
}

interface Mensagem {
    id: number;
    assunto_id: string;
    user_id: string;
    mensagem: string;
    pendente: string;
    arquivado: string;
    created_at: string;
    updated_at: string;
    deleted_at?: any;
}