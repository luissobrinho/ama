export interface Assuntos {
    current_page: number;
    data: Assunto[];
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: string;
    next_page_url?: any;
    path: string;
    per_page: number;
    prev_page_url?: any;
    to: number;
    total: number;
}

export interface Assunto {
    id: number;
    nome: string;
    slug: string;
    created_at?: any;
    updated_at?: any;
    deleted_at?: any;
}