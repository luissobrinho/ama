export interface Profile {
  user: UserMeta;
}

interface UserMeta {
  id: number;
  name: string;
  last_name: string;
  username: string;
  email: string;
  email_verified_at?: any;
  last_login: string;
  created_at: string;
  updated_at: string;
  deleted_at?: any;
  meta: Meta;
}

interface Meta {
  id: number;
  user_id: string;
  phone?: any;
  is_active: string;
  activation_token?: any;
  marketing: string;
  terms_and_cond: string;
  created_at: string;
  updated_at: string;
  municipio_id: string;
  cargo_id: string;
  partido_id: string;
  data_nascimento: string;
  municipio: Municipio;
  cargo: Cargo;
}

interface Municipio {
  id: number;
  nome: string;
  cnpj: string;
  data_emancipacao: string;
  site: string;
  email: string;
  telefone: string;
  fpm?: any;
  ibge: string;
  created_at?: any;
  updated_at: string;
  deleted_at?: any;
  usuarios: Usuario[];
  populacoes: any[];
}

interface Usuario {
  id: number;
  user_id: string;
  phone?: any;
  is_active: string;
  activation_token?: any;
  marketing: string;
  terms_and_cond: string;
  created_at: string;
  updated_at: string;
  municipio_id: string;
  cargo_id: string;
  partido_id: string;
  data_nascimento: string;
  user: User;
  cargo: Cargo;
  partido: Partido;
}

interface Partido {
  id: number;
  nome: string;
  sigla: string;
  legenda: string;
  created_at?: any;
  updated_at?: any;
}

interface Cargo {
  id: number;
  nome: string;
  ordem: string;
  created_at?: any;
  updated_at?: any;
}

interface User {
  id: number;
  name: string;
  last_name: string;
  username: string;
  email: string;
  email_verified_at?: any;
  last_login: string;
  created_at: string;
  updated_at: string;
  deleted_at?: any;
}