
export interface Noticias {
    current_page: number;
    data: Noticia[];
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: string;
    next_page_url?: any;
    path: string;
    per_page: number;
    prev_page_url?: any;
    to: number;
    total: number;
}

export interface Noticia {
    id: number;
    categoria_id: string;
    titulo: string;
    slug: string;
    subtitulo?: any;
    foto: string;
    data: string;
    texto: string;
    created_at: string;
    updated_at: string;
    deleted_at?: any;
    categoria: Categoria;
}

interface Categoria {
    id: number;
    nome: string;
    slug: string;
    created_at?: string;
    updated_at?: string;
    deleted_at?: any;
}