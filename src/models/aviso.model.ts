export interface Aviso {
    id: number;
    data: string;
    mensagem: string;
    enviado: string;
    created_at: string;
    updated_at: string;
    deleted_at?: any;
}