export interface MensagemCurrent {
    id: number;
    assunto_id: string;
    user_id: string;
    mensagem: string;
    pendente: string;
    arquivado: string;
    created_at: string;
    updated_at: string;
    deleted_at?: any;
    respostas: Resposta[];
}

interface Resposta {
    id: number;
    mensagem_id: string;
    user_id: string;
    resposta: string;
    created_at: string;
    updated_at: string;
    usuario: Usuario;
}

interface Usuario {
    id: number;
    name: string;
    last_name: string;
    username: string;
    email: string;
    email_verified_at?: any;
    last_login: string;
    created_at: string;
    updated_at: string;
    deleted_at?: any;
}