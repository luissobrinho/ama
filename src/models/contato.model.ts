export interface Contatos {
  current_page: number;
  data: Contato[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url?: any;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface Contato {
  id: number;
  name: string;
  last_name: string;
  username: string;
  email: string;
  email_verified_at?: any;
  last_login: string;
  created_at: string;
  updated_at: string;
  deleted_at?: any;
  roles: any[];
  meta: Meta;
}

interface Meta {
  id: number;
  user_id: string;
  phone?: any;
  is_active: string;
  activation_token?: any;
  marketing: string;
  terms_and_cond: string;
  created_at: string;
  updated_at: string;
  municipio_id?: string;
  cargo_id?: string;
  partido_id?: string;
  data_nascimento?: string;
}