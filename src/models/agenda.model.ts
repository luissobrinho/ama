export interface Agendas {
  current_page: number;
  data: Agenda[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url?: any;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface Agenda {
  id: number;
  evento: string;
  local: string;
  data_inicio: string;
  data_fim: string;
  dia_todo: string;
  publicar: string;
  created_at: string;
  updated_at: string;
  deleted_at?: any;
}