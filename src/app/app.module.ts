import { HomePopoverPage } from './../pages/home/home-popover';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthProvider } from '../providers/auth/auth';
import { ApiProvider } from '../providers/api/api';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { NoticiaProvider } from '../providers/noticia/noticia';
import { PautaProvider } from '../providers/pauta/pauta';
import { AgendaProvider } from '../providers/agenda/agenda';
import { MunicipioProvider } from '../providers/municipio/municipio';
import { ContatoProvider } from '../providers/contato/contato';
import { AvisoProvider } from '../providers/aviso/aviso';
import { FaleConoscoProvider } from '../providers/fale-conosco/fale-conosco';

import { OneSignal } from '@ionic-native/onesignal';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    HomePopoverPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: 'ios',
      iconMode: 'ios',
      platforms: {
        ios: {
          backButtonText: ''
        }
      }
    }),
    HttpClientModule,
    IonicStorageModule.forRoot({
      name: '__AMA'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    HomePopoverPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthProvider,
    ApiProvider,
    NoticiaProvider,
    PautaProvider,
    AgendaProvider,
    MunicipioProvider,
    ContatoProvider,
    AvisoProvider,
    FaleConoscoProvider,
    OneSignal
  ]
})
export class AppModule { }
