import { Profile } from './../models/user.model';
import { AuthProvider } from './../providers/auth/auth';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Platform, AlertOptions, LoadingOptions, ActionSheetOptions, AlertController, LoadingController, Events, MenuController, ToastController, ActionSheetController, Loading, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from '@ionic-native/onesignal';

import moment from 'moment';

import { HomePage } from '../pages/home/home';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment.prod';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = null;
  loading: Loading;
  @ViewChild(Nav) nav: Nav;
  _user: Profile;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    authProvider: AuthProvider,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public events: Events,
    public menuController: MenuController,
    public toastController: ToastController,
    public actionSheetController: ActionSheetController,
    private oneSignal: OneSignal,
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      splashScreen.hide();
      moment.locale('pt-br');
      if (environment.production) {
        this.oneSignal.startInit('09d65a3e-0a96-4d73-8c65-e07073684b65', '484547379791'); //MEU
        this.oneSignal.startInit('270a6874-b810-467f-bd30-23f996d65e83', '52717913030');

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
          // do something when notification is received
        });

        this.oneSignal.handleNotificationOpened()
          .subscribe((notify) => {
            // do something when a notification is opened
            if (notify.notification.payload.actionButtons[0].id == 'noticia') {
              this.openPage('NoticiaPage', { notificacao: notify });
            }
          });
      }

      authProvider.isAuth()
        .then((is: boolean) => {
          if (is) {
            statusBar.backgroundColorByHexString('#3A6FBD');
            this.rootPage = HomePage;
          } else {
            statusBar.backgroundColorByHexString('#052554');
            this.rootPage = LoginPage;
          }
        }).catch(() => {
          statusBar.backgroundColorByHexString('#052554');
          this.rootPage = LoginPage;
        });
      this.loadEvents();

      this.oneSignal.endInit();
    });

  }

  loadEvents() {
    // Loading
    this.events.subscribe('loading:create', (options: LoadingOptions) => {
      this.createAndOpenLoadin(options);
    });
    this.events.subscribe('loading:dismiss', () => {
      this.dismissLoading();
    });

    // Toast
    this.events.subscribe('toast:error', (err: HttpErrorResponse) => {
      this.toastError(err);
    })
    this.events.subscribe('toast:create', (msg: string) => {
      this.toastCreate(msg);
    })

    // Alert
    this.events.subscribe('alert:create', (options: AlertOptions) => {
      this.alertMessage(options);
    });

    // Page
    this.events.subscribe('page:open', (page: string, params: any) => {
      this.openPage(page, params);
    });
    this.events.subscribe('page:root', () => {
      this.nav.popToRoot();
    });
    this.events.subscribe('page:pop', () => {
      this.nav.pop();
    });

    // Auth
    this.events.subscribe('auth:login', () => {
      this.rootPage = HomePage;
    });
    this.events.subscribe('auth:logout', () => {
      this.rootPage = LoginPage;
    });
    this.events.subscribe('auth:me', (user: Profile) => {
      this._user = user;
      this.oneSignal.sendTag('municipio', user.user.meta.municipio.nome);
    });

    // ActionSheet
    this.events.subscribe('actionSheet:create', (action: ActionSheetOptions) => {
      this.presentActionSheet(action);
    })

  }

  createAndOpenLoadin(options: LoadingOptions) {
    this.loading = this.loadingController.create(options);
    this.loading.present();
  }

  setContentLoading(content: string) {
    this.loading.setContent(content);
  }

  dismissLoading() {
    this.loading.dismiss();
  }

  alertMessage(options: AlertOptions) {
    this.alertController.create(options).present();
  }

  toastError(err: HttpErrorResponse) {
    let msg = ``;
    for (var error in err.error.errors) {
      err.error.errors[error].forEach((m) => {
        msg += `${m}`
      });
    };
    this.toastCreate(msg);
  }

  toastCreate(msg: string) {
    this.toastController.create({
      message: msg,
      duration: 3000,
      position: 'botton',
      showCloseButton: true,
      closeButtonText: 'FECHAR'
    }).present();
  }

  presentActionSheet(action: ActionSheetOptions) {
    this.actionSheetController
      .create(action)
      .present();
  }

  openPage(page: string, params = null) {
    this.nav.push(page, params);
  }
}