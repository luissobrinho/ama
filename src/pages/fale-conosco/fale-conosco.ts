import { FaleConoscoProvider } from './../../providers/fale-conosco/fale-conosco';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http';
import { MensagemCurrent } from '../../models/mensagem.model';

/**
 * Generated class for the FaleConoscoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fale-conosco',
  templateUrl: 'fale-conosco.html',
})
export class FaleConoscoPage {
  mensagens: Mensagem[] = [];

  constructor(
    public events: Events,
    public faleConoscoProvider: FaleConoscoProvider
  ) {
    this.events.publish('loading:create', { content: 'Carregando...' });
    this.faleConoscoProvider.index().then((mensagens: Mensagens) => {
      this.mensagens = mensagens.data;
      this.events.publish('loading:dismiss');
    }).catch((err: HttpErrorResponse) => {
      this.events.publish('loading:dismiss');
    });

    this.events.subscribe('mensagem:reload', () => {
      this.events.publish('loading:create', { content: 'Carregando...' });
      this.faleConoscoProvider.index().then((mensagens: Mensagens) => {
        this.mensagens = mensagens.data;
        this.events.publish('loading:dismiss');
      }).catch((err: HttpErrorResponse) => {
        this.events.publish('loading:dismiss');
      });
    });
  }

  ionViewWillUnload() {
    this.events.unsubscribe('mensagem:reload');
  }

  openMensagem(id: number) {
    this.events.publish('loading:create', { content: 'Buscando...' });
    this.faleConoscoProvider.show(id)
      .then((mensagen: MensagemCurrent) => {
        this.events.publish('loading:dismiss');
        this.events.publish('page:open', 'FaleConoscoDetalhesPage', { mensagem: mensagen });
      }).catch((err: HttpErrorResponse) => {
        this.events.publish('loading:dismiss');
      });
  }

  back() {
    this.events.publish('page:pop');
  }

  openNovo() {
    this.events.publish('page:open', 'FaleConoscoNovoPage');
  }
}
