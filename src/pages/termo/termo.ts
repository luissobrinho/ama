import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';

/**
 * Generated class for the TermoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-termo',
  templateUrl: 'termo.html',
})
export class TermoPage {

  constructor(
    public events: Events
  ) {
  }

  back() {
    this.events.publish('page:pop');
  }

}
