import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendaDiaPage } from './agenda-dia';

@NgModule({
  declarations: [
    AgendaDiaPage,
  ],
  imports: [
    IonicPageModule.forChild(AgendaDiaPage),
  ],
})
export class AgendaDiaPageModule {}
