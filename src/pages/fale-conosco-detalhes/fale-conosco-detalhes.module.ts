import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaleConoscoDetalhesPage } from './fale-conosco-detalhes';

@NgModule({
  declarations: [
    FaleConoscoDetalhesPage,
  ],
  imports: [
    IonicPageModule.forChild(FaleConoscoDetalhesPage),
  ],
})
export class FaleConoscoDetalhesPageModule {}
