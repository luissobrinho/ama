import { FaleConoscoProvider } from './../../providers/fale-conosco/fale-conosco';
import { AuthProvider } from './../../providers/auth/auth';
import { MensagemCurrent } from './../../models/mensagem.model';
import { Component } from '@angular/core';
import { IonicPage, NavParams, Events } from 'ionic-angular';
import { Profile } from '../../models/user.model';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Generated class for the FaleConoscoDetalhesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fale-conosco-detalhes',
  templateUrl: 'fale-conosco-detalhes.html',
})
export class FaleConoscoDetalhesPage {
  mensagem: MensagemCurrent;
  user: Profile;
  resposta: string = '';

  constructor(
    public navParams: NavParams,
    public authProvider: AuthProvider,
    public events: Events,
    public faleConoscoProvider: FaleConoscoProvider
  ) {
    this.user = authProvider.user();
    this.mensagem = <MensagemCurrent>this.navParams.get('mensagem');
  }

  send() {
    this.events.publish('loading:create', { content: 'Enviado...' });
    this.faleConoscoProvider.reply({
      mensagem_id: this.mensagem.id,
      resposta: this.resposta
    })
      .then(() => {
        this.resposta = '';
        this.faleConoscoProvider.show(this.mensagem.id)
          .then((mensagem: MensagemCurrent) => {
            this.mensagem = mensagem;
            this.events.publish('loading:dismiss');
          }).catch(() => {
            this.events.publish('loading:dismiss');
            this.events.publish('toast:create', 'Erro ao lista mensagem');
          })
      }).catch((err: HttpErrorResponse) => {
        this.events.publish('toast:create', 'Erro ao enviar mensagem');
        this.events.publish('loading:dismiss');
      });
  }

  back() {
    this.events.publish('page:pop');
  }

}
