import { Assuntos, Assunto } from './../../models/assunto.model';
import { HttpErrorResponse } from '@angular/common/http';
import { FaleConoscoProvider } from './../../providers/fale-conosco/fale-conosco';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';

/**
 * Generated class for the FaleConoscoNovoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fale-conosco-novo',
  templateUrl: 'fale-conosco-novo.html',
})
export class FaleConoscoNovoPage {
  faleconoscoForm: FormGroup;
  assuntos: Assunto[] = [];
  constructor(
    public formBuilder: FormBuilder,
    public events: Events,
    public faleConoscoProvider: FaleConoscoProvider
  ) {
    this.faleconoscoForm = formBuilder.group({
      assunto_id: ['', [Validators.required]],
      mensagem: ['', [Validators.required]]
    });
    
    this.events.publish('loading:create', { content: 'Carregando...' });
    this.faleConoscoProvider.assunto()
      .then((assuntos: Assuntos) => {
        this.events.publish('loading:dismiss');
        this.assuntos = assuntos.data;
      });
  }

  back() {
    this.events.publish('page:pop');
  }

  onSubmit() {
    this.events.publish('loading:create', { content: 'Enviado...' });
    this.faleConoscoProvider
      .store(this.faleconoscoForm.value)
      .then(() => {
        this.events.publish('loading:dismiss');
        this.events.publish('alert:create', {
          subTitle: 'Sua mensagem enviada!',
          buttons: [{
            text: 'OK',
            handler: () => {
              this.events.publish('page:pop');
              this.events.publish('mensagem:reload');
            }
          }]
        });
      }).catch((err: HttpErrorResponse) => {
        this.events.publish('loading:dismiss');
        if (err.status == 422) {
          this.events.publish('toast:error', err);
        } else {
          this.events.publish('toast:create', 'Houve um erro ao salvar sua mensagem.');
        }
      });
  }
}
