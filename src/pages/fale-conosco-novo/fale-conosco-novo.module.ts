import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaleConoscoNovoPage } from './fale-conosco-novo';

@NgModule({
  declarations: [
    FaleConoscoNovoPage,
  ],
  imports: [
    IonicPageModule.forChild(FaleConoscoNovoPage),
  ],
})
export class FaleConoscoNovoPageModule {}
