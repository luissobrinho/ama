import { Aviso } from './../../models/aviso.model';
import { AvisoProvider } from './../../providers/aviso/aviso';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Generated class for the AvisoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-aviso',
  templateUrl: 'aviso.html',
})
export class AvisoPage {
  avisos: Aviso[] = [];

  constructor(
    public events: Events,
    public avisoProvider: AvisoProvider
  ) {
    this.events.publish('loading:create', { content: 'Carregando...' });
    this.avisoProvider.index().then((avisos: Aviso[]) => {
      this.avisos = avisos;
      this.events.publish('loading:dismiss');
    }).catch((err: HttpErrorResponse) => {
      this.events.publish('loading:dismiss');
    });
  }
  index() {
    this.avisoProvider.index().then((avisos: Aviso[]) => {
      this.avisos = avisos;
    }).catch((err: HttpErrorResponse) => {
    });
  }

  read(id: number) {
    this.avisoProvider.read(id).then((d) => console.log(d)).catch((e) => console.log(e));
    this.events.publish('aviso:reload');
    this.index();
  }

  back() {
    this.events.publish('page:pop');
  }

}
