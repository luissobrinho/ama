import { CurrentMinicipio } from './../../models/municipio.show.model';
import { Municipios, Municipio } from './../../models/monicipio.model';
import { MunicipioProvider } from './../../providers/municipio/municipio';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Generated class for the MunicipioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-municipio',
  templateUrl: 'municipio.html',
})
export class MunicipioPage {
  municipio: number = 0;
  municipios: Municipio[];
  current: CurrentMinicipio = {};
  prefeito: any
  viceprefeito: any
  isCurrent: boolean = false;

  constructor(public events: Events, public municipioProvider: MunicipioProvider) {
    this.events.publish('loading:create', { content: 'Carregando...' });
    this.municipioProvider.index()
      .then((municipios: Municipios) => {
        this.municipios = municipios.data;
        this.events.publish('loading:dismiss');
      }).catch((err: HttpErrorResponse) => {
        this.events.publish('loading:dismiss');
      });
  }

  selectMunicipio() {
    this.events.publish('loading:create', { content: 'Buscando...' });
    this.municipioProvider.show(this.municipio)
      .then((municipio: CurrentMinicipio) => {
        this.current = municipio;
        this.prefeito = municipio.usuarios.find(u => u.cargo.ordem == '1');
        this.viceprefeito = municipio.usuarios.find(u => u.cargo.ordem == '2');
        this.events.publish('loading:dismiss');
        this.isCurrent = true;
      }).catch((err: HttpErrorResponse) => {
        this.isCurrent = false;
        this.events.publish('loading:dismiss');
      });
  }

  back() {
    this.events.publish('page:pop');
  }
}
