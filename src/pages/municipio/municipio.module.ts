import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MunicipioPage } from './municipio';

@NgModule({
  declarations: [
    MunicipioPage,
  ],
  imports: [
    IonicPageModule.forChild(MunicipioPage),
  ],
})
export class MunicipioPageModule {}
