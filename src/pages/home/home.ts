import { HttpErrorResponse } from '@angular/common/http';
import { Aviso } from './../../models/aviso.model';
import { AvisoProvider } from './../../providers/aviso/aviso';
import { Component } from '@angular/core';
import { NavController, Events, PopoverController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Profile } from '../../models/user.model';
import { HomePopoverPage } from './home-popover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  profile: Profile;
  cargo: string = '';
  hasAviso: boolean = false;
  quantidadeAviso: number = 0;

  constructor(
    statusBar: StatusBar,
    public navCtrl: NavController,
    public events: Events,
    public avisoProvider: AvisoProvider,
    public popoverCtrl: PopoverController
  ) {
    statusBar.backgroundColorByHexString('#3a6fbd');
    this.events.subscribe('auth:me', (profile: Profile) => {
      this.profile = profile;
      this.cargo = `${profile.user.meta.cargo.nome.slice(0, 4).toLocaleUpperCase()}. DE ${profile.user.meta.municipio.nome.toLocaleUpperCase()}`;
    });

    this.hasAvisoIndex();

    this.events.subscribe('aviso:reload', () => {
      this.hasAvisoIndex();
    });
  }

  hasAvisoIndex() {
    this.avisoProvider.index().then((avisos: Aviso[]) => {
      this.hasAviso = (avisos.length > 0);
      this.quantidadeAviso = avisos.length;
    }).catch((err: HttpErrorResponse) => {

    });
  }

  logout() {
    this.events.publish('user:logout');
  }

  openPage(page: string) {
    this.events.publish('page:open', page)
  }

  openContato(page: string, categoria_id: number) {
    this.events.publish('page:open', page, { categoria_id: categoria_id })
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(HomePopoverPage);
    popover.present({
      ev: myEvent
    });
  }

}
