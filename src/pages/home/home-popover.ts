import { Component } from "@angular/core";
import { ViewController, Events } from "ionic-angular";

@Component({
    template: `
      <ion-list no-margin class="normal">
        <button ion-item (click)="aviso()">Avisos</button>
        <button ion-item (click)="termo()">Termo de Uso</button>
        <button ion-item (click)="exit()">Sair</button>
      </ion-list>
    `
})
export class HomePopoverPage {
    constructor(public viewCtrl: ViewController, public events: Events) { }

    exit() {
        this.viewCtrl.dismiss();
        this.events.publish('user:logout');
    }

    termo() {
        this.viewCtrl.dismiss();
        this.events.publish('page:open', 'TermoPage');
    }
    
    aviso() {
        this.viewCtrl.dismiss();
        this.events.publish('page:open', 'AvisoPage');
    }
}