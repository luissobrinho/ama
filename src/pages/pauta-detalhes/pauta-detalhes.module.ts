import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PautaDetalhesPage } from './pauta-detalhes';

@NgModule({
  declarations: [
    PautaDetalhesPage,
  ],
  imports: [
    IonicPageModule.forChild(PautaDetalhesPage),
  ],
})
export class PautaDetalhesPageModule {}
