import { Pauta } from './../../models/pauta.model';
import { PautaProvider } from './../../providers/pauta/pauta';
import { Component } from '@angular/core';
import { IonicPage, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the PautaDetalhesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pauta-detalhes',
  templateUrl: 'pauta-detalhes.html',
})
export class PautaDetalhesPage {
  pauta: Pauta;

  constructor(
    public navParams: NavParams,
    public events: Events,
    public pautaProvider: PautaProvider
  ) {
    this.pauta = <Pauta>this.navParams.get('pauta');

  }
  back() {
    this.events.publish('page:pop');
  }

}
