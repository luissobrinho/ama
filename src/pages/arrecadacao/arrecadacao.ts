import { FormGroup, FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';

/**
 * Generated class for the ArrecadacaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-arrecadacao',
  templateUrl: 'arrecadacao.html',
})
export class ArrecadacaoPage {
  arrecadacaoForm: FormGroup
  constructor(public formBuilder: FormBuilder, public events: Events) {
    this.arrecadacaoForm = this.formBuilder.group({

    });
  }

  back() {
    this.events.publish('page:pop');
  }

  onSubmit() {
    this.events.publish('loading:create', { content: 'Buscando...' });
    let interval = setInterval(() => {
      this.events.publish('loading:dismiss');
      this.events.publish('toast:create', 'Função não implementada!');
      clearInterval(interval)
    }, 2000);
  }

}
