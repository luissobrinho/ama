import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArrecadacaoPage } from './arrecadacao';

@NgModule({
  declarations: [
    ArrecadacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ArrecadacaoPage),
  ],
})
export class ArrecadacaoPageModule {}
