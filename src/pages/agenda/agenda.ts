import { HttpErrorResponse } from '@angular/common/http';
import { AgendaProvider } from './../../providers/agenda/agenda';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import * as moment from 'moment';
import { Agendas, Agenda } from '../../models/agenda.model';

/**
 * Generated class for the AgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
})
export class AgendaPage {
  view: string = 'ano';
  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[] = ['JANEIRO', 'FEVEREIRO', 'MARÇO', 'ABRIL', 'MAIO', 'JUNHO', 'JULHO', 'AGOSTO', 'SETEMBRO', 'OUTUBRO', 'NOVEMBRO', 'DEZEMBRO'];
  currentMonth: any;
  currentYear: any;
  currentDate: any;

  dateyear: {
    daysInThisMonth: any,
    daysInLastMonth: any,
    daysInNextMonth: any,
    currentMonth: string,
    currentDate: any
  }[] = []
  agendas: Agenda[] = [];


  constructor(
    public events: Events,
    public agendaProvider: AgendaProvider
  ) {
    this.events.publish('loading:create', { content: 'Carregando...' });
    this.agendaProvider.index().then((agendas: Agendas) => {
      this.agendas = agendas.data;
      this.events.publish('loading:dismiss');
      for (let index = 0; index < 12; index++) {
        this.getDaysOfMonth(new Date(2019, index, 1), false);
      }
      this.getDaysOfMonth(new Date(), true)
    }).catch((err: HttpErrorResponse) => {
      this.events.publish('loading:dismiss');
    });
  }

  getDaysOfMonth(date: Date, current: boolean) {
    let daysInThisMonth = new Array();
    let daysInLastMonth = new Array();
    let daysInNextMonth = new Array();
    let currentDate = null;
    let currentMonth = this.monthNames[date.getMonth()];
    
    this.currentYear = date.getFullYear();
    if (date.getMonth() === new Date().getMonth() && date.getFullYear() === new Date().getFullYear()) {
      currentDate = new Date().getDate();
    } else {
      currentDate = 999;
    }

    var firstDayThisMonth = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(date.getFullYear(), date.getMonth(), 0).getDate();
    for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      daysInLastMonth.push(i);
    }

    var thisNumOfDays = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
    for (var i = 0; i < thisNumOfDays; i++) {
      daysInThisMonth.push(i + 1);
    }

    var lastDayThisMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDay();
    for (var i = 0; i < (6 - lastDayThisMonth); i++) {
      daysInNextMonth.push(i + 1);
    }
    var totalDays = daysInLastMonth.length + daysInThisMonth.length + daysInNextMonth.length;
    if (totalDays < 36) {
      for (var i = (7 - lastDayThisMonth); i < ((7 - lastDayThisMonth) + 7); i++) {
        daysInNextMonth.push(i);
      }
    }

    if (current) {
      this.daysInLastMonth = daysInLastMonth;
      this.daysInNextMonth = daysInNextMonth;
      this.daysInThisMonth = daysInThisMonth;
      this.currentMonth = currentMonth;
      this.currentDate = currentDate;
    } else {
      this.dateyear.push({
        daysInLastMonth: daysInLastMonth,
        daysInNextMonth: daysInNextMonth,
        daysInThisMonth: daysInThisMonth,
        currentMonth: currentMonth,
        currentDate: currentDate
      });
    }

  }

  back() {
    this.events.publish('page:pop');
  }

  viewMes(date: { daysInThisMonth: any; daysInLastMonth: any; daysInNextMonth: any; currentMonth: string; currentDate: any; }) {
    this.daysInLastMonth = date.daysInLastMonth;
    this.daysInNextMonth = date.daysInNextMonth;
    this.daysInThisMonth = date.daysInThisMonth;
    this.currentMonth = date.currentMonth;
    this.currentDate = date.currentDate;
    this.view = 'mes';
  }

  isEvent(day: number) {
    let is = false;
    this.agendas.forEach(agenda => {
      let _month = (this.monthNames.indexOf(this.currentMonth) + 1 < 10)
        ? '0' + (this.monthNames.indexOf(this.currentMonth) + 1)
        : (this.monthNames.indexOf(this.currentMonth) + 1).toString();
      let _day = (day < 10) ? '0' + day : day;
      if (
        moment(`${this.currentYear}-${_month}-${_day} 00:00:00`)
          .isBetween(moment(agenda.data_inicio), moment(agenda.data_fim), 'days', '[)')
      ) {
        is = true;
      }
    });

    return is;
  }

  viewDay(day: number) {

    let message = '';
    this.agendas.forEach(agenda => {
      let _month = (this.monthNames.indexOf(this.currentMonth) + 1 < 10)
        ? '0' + (this.monthNames.indexOf(this.currentMonth) + 1)
        : (this.monthNames.indexOf(this.currentMonth) + 1).toString();
      let _day = (day < 10) ? '0' + day : day;
      if (
        moment(`${this.currentYear}-${_month}-${_day} 00:00:00`)
          .isBetween(moment(agenda.data_inicio), moment(agenda.data_fim), 'days', '[)')
      ) {
        message += `<strong>Evento:</strong> ${agenda.evento} <br><strong>Local:</strong> ${agenda.local} <br> <br>`;
      }
    });
    this.events.publish('alert:create', {
      message: message,
      buttons: ['OK']
    });
  }

}
