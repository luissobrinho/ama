import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendaPage } from './agenda';

@NgModule({
  declarations: [
    AgendaPage,
  ],
  imports: [ 
    IonicPageModule.forChild(AgendaPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AgendaPageModule {}
