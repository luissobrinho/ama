import { HomePage } from './../home/home';
import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Events, NavController } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    public authProvider: AuthProvider,
    public navController: NavController,
    public events: Events
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
    // this.authProvider.setToken(`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6Ly9hbWEuanBhc3Nlc3NvcmlhLm1lL2FwaS92MS9sb2dpbiIsImlhdCI6MTU1NzY3NDM0OSwiZXhwIjoxNTg5MjEwMzQ5LCJuYmYiOjE1NTc2NzQzNDksImp0aSI6IlkxTmNkRThmcWVqMEpYaXQifQ.97X6Bu8wiNXcm246Uo5_Z3H-HarxojbxTwrwW2LP3aQ`).then(() => {
    //   this.navController.setRoot(HomePage);
    // });
  }

  onSubmit() {
    this.events.publish('loading:create', { content: 'Autenticando...' });
    this.authProvider.login(this.loginForm.value)
      .then((data: { token: string }) => {
        this.events.publish('loading:dismiss');
        this.authProvider.setToken(data.token).then(() => {
          this.events.publish('auth:login', data);
          this.navController.setRoot(HomePage);

        });
      }).catch((err: HttpErrorResponse) => {
        this.events.publish('loading:dismiss');
        this.events.publish('toast:create', 'Erro ao tentar se autenticar!');
      });
  }

}
