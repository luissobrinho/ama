import { Noticia } from './../../models/noticia.model';
import { Component } from '@angular/core';
import { IonicPage, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the NoticiaDetalhesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noticia-detalhes',
  templateUrl: 'noticia-detalhes.html', 
})
export class NoticiaDetalhesPage {
  noticia: Noticia;

  constructor(public navParams: NavParams, public events: Events) {
    this.noticia = <Noticia>this.navParams.get('noticia')
  }

  back() {
    this.events.publish('page:pop');
  }
}
