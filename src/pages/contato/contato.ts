import { Contatos, Contato } from './../../models/contato.model';
import { ContatoProvider } from './../../providers/contato/contato';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Generated class for the ContatoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contato',
  templateUrl: 'contato.html',
})
export class ContatoPage {
  contatos: Contato[] = [];

  constructor(public events: Events, public contatoProvider: ContatoProvider) {
    this.index();
  }

  index() {
    this.events.publish('loading:create', { content: 'Carregando...' });
    this.contatoProvider.index().then((contatos: Contatos) => {
      this.contatos = contatos.data;
      this.events.publish('loading:dismiss');
    }).catch((err: HttpErrorResponse) => {
      this.events.publish('loading:dismiss');
    });
  }

  doRefresh(refresher) {
    this.index();
    refresher.complete();
  }

  back() {
    this.events.publish('page:pop');
  }
}
