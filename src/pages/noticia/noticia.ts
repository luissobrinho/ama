import { Categorias, Categoria } from './../../models/categoria.model';
import { Noticias, Noticia } from './../../models/noticia.model';
import { NoticiaProvider } from './../../providers/noticia/noticia';
import { Component } from '@angular/core';
import { IonicPage, Events, NavParams } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Generated class for the NoticiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noticia',
  templateUrl: 'noticia.html',
})
export class NoticiaPage {
  noticias: Noticia[] = [];
  noticias_pag: Noticias;
  list_categoria: boolean = true;
  categorias: Categoria[] = [];
  categoria_id: string = '';

  constructor(
    public events: Events,
    public noticiaProvider: NoticiaProvider,
    public navParams: NavParams
  ) {
    if (this.navParams.get('categoria_id')) {
      this.categoria_id = this.navParams.get('categoria_id');
      this.listByCategoria();
      this.list_categoria = false;
    } else if (this.navParams.get('notificacao')) {
      let notify = this.navParams.get('notificacao');
      this.events.publish('loading:create', { content: 'Aguarde...' });

      this.noticiaProvider.show(parseInt(notify.notification.payload.additionalData.noticia_id))
        .then((noticia: Noticia) => {
          this.events.publish('loading:dismiss');
          this.indexAll(false);
          this.openNoticia(noticia);
        }).catch((err: HttpErrorResponse) => {
          this.events.publish('loading:dismiss');
          this.events.publish('toast:create', 'Erro ao tentar abrir a notícia!');
          this.indexAll(false);
        });
    } else {
      this.indexAll(true);
    }
  }

  indexAll(withLoading: boolean) {
    (withLoading) ? this.events.publish('loading:create', { content: 'Carregando...' }) : null;
    this.noticiaProvider.index()
      .then((noticias: Noticias) => {
        (withLoading) ? this.events.publish('loading:dismiss') : null;
        this.noticias = noticias.data;
        this.noticias_pag = noticias;
      }).catch(() => {
        (withLoading) ? this.events.publish('loading:dismiss') : null;
        this.events.publish('toast:create', 'Nenhuma notícia encontrada!');
      });

    this.noticiaProvider.indexCategoria()
      .then((categorias: Categorias) => {
        this.categorias = categorias.data;
      }).catch(() => {

      })
  }

  openNoticia(noticia: Noticia) {
    this.events.publish('page:open', 'NoticiaDetalhesPage', { noticia: noticia });
  }

  listByCategoria() {
    this.events.publish('loading:create', { content: 'Carregando...' });
    this.noticiaProvider.byCategoria(parseInt(this.categoria_id))
      .then((noticias: Noticia[]) => {
        this.events.publish('loading:dismiss');
        if (noticias.length > 0) {
          this.noticias = noticias;
        } else {
          this.events.publish('toast:create', 'Nenhuma notícia encontrada!');
        }
      }).catch(() => {
        this.events.publish('loading:dismiss');
        this.events.publish('toast:create', 'Nenhuma notícia encontrada!');
      });
  }

  back() {
    this.events.publish('page:pop');
  }

}
