import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PautaPage } from './pauta';

@NgModule({
  declarations: [
    PautaPage,
  ],
  imports: [
    IonicPageModule.forChild(PautaPage),
  ],
})
export class PautaPageModule {}
