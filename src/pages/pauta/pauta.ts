import { HttpErrorResponse } from '@angular/common/http';
import { Pautas, Pauta } from './../../models/pauta.model';
import { PautaProvider } from './../../providers/pauta/pauta';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';

/**
 * Generated class for the PautaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pauta',
  templateUrl: 'pauta.html',
})
export class PautaPage {
  pautas: Pauta[] = [];
  pautas_page: Pautas;

  constructor(
    public events: Events,
    public pautaProvider: PautaProvider
  ) {
    this.pautaProvider.index()
      .then((pautas: Pautas) => {
        this.pautas = pautas.data;
        this.pautas_page = pautas;
      }).catch((err: HttpErrorResponse) => {
        if (err.status === 404) {
          this.events.publish('toast:create', 'Nenhuma pauta cadastrada!');
        }
      });
  }

  back() {
    this.events.publish('page:pop');
  }

  openDetalhes(id: number) {
    this.events.publish('loading:create', {
      content: 'Carregando...'
    });
    this.pautaProvider.show(id)
      .then((pauta: Pauta) => {
        this.events.publish('loading:dismiss');
        this.events.publish('page:open', 'PautaDetalhesPage', { pauta: pauta });
      }).catch((err: HttpErrorResponse) => {
        this.events.publish('loading:dismiss');
        this.events.publish('toast:create', 'Pauta não encontrada!');
      });
  }

}
