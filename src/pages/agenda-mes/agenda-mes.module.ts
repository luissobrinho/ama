import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendaMesPage } from './agenda-mes';

@NgModule({
  declarations: [
    AgendaMesPage,
  ],
  imports: [
    IonicPageModule.forChild(AgendaMesPage),
  ],
})
export class AgendaMesPageModule {}
